from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Activity, Member

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.activity_url = reverse("story6app:activity")
        self.test_activity = Activity.objects.create(
            activity = "menutut_ilmu"
        )
        self.test_member = Member.objects.create(
            member = "Qonita",
            aktivitas = self.test_activity
        )
        self.test_delete_url = reverse("story6app:deleteActivity",args=[self.test_activity.id])
        self.test_member_delete_url = reverse("story6app:deleteMember",args=[self.test_member.id])

    def test_activity_GET(self):
        response = self.client.get(self.activity_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "story6app/index.html")

    def test_activity_POST(self):
        response = self.client.post(self.activity_url, {
            "activity" : "belajar"
        }, follow=True)
        self.assertContains(response, "belajar")

    def test_member_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "upi",
            "aktivitas" : self.test_activity.id,
        }, follow=True)
        self.assertContains(response, "upi")

    def test_notValid_POST(self):
        response = self.client.post(self.activity_url, {
            "member" : "upi",
            "aktivitas" : "tawain",
        }, follow=True)
        self.assertContains(response, "Input tidak sesuai")

    def test_member_DELETE(self):
        response = self.client.get(self.test_member_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")

    def test_activity_DELETE(self):
        response = self.client.get(self.test_delete_url, follow=True)
        self.assertContains(response, "berhasil dihapus")