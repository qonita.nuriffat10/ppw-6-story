from django.urls import path
from . import views 
app_name = 'story6app'

urlpatterns = [
    path('', views.activity, name='activity'),
    path('delete/<str:pk>', views.deleteActivity, name='deleteActivity'),
    path('delete/member/<str:pk>', views.deleteMember, name='deleteMember')
]

